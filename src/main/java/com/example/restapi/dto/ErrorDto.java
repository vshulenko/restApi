package com.example.restapi.dto;

import lombok.Data;

import java.util.List;

@Data
public class ErrorDto {
    //test1
    //test2,something great and also
    //test3
    List<String> messages;
    String exceptionName;
}
